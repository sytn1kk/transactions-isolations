# Transaction 1

START TRANSACTION;

SELECT Balance INTO @balance FROM app.BankDetailTbl WHERE id = 1;

DO SLEEP(20);

UPDATE app.BankDetailTbl
SET Balance = @balance + 5
WHERE id = 1;

COMMIT;

SELECT * FROM app.BankDetailTbl WHERE id = 1;

# Transaction 2

START TRANSACTION;

SELECT Balance INTO @balance FROM app.BankDetailTbl WHERE id = 1;

UPDATE app.BankDetailTbl
SET Balance = Balance + 7
WHERE id = 1;

COMMIT ;

SELECT * FROM app.BankDetailTbl WHERE id = 1;