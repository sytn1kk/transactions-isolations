# Transaction 1

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;

SELECT Balance FROM app.BankDetailTbl WHERE id = 1;

DO SLEEP(10);

SELECT Balance FROM app.BankDetailTbl WHERE id = 1;

COMMIT;

# Transaction 2

START TRANSACTION;

UPDATE app.BankDetailTbl
SET Balance = 42
WHERE id = 1;

COMMIT;