# Transactions And Isolations
## MySQL

## Screenshots
### Dirty read
![Dirty read](https://i2.paste.pics/988ac2d424e85fae33e866e486aa884e.png "Dirty read")
### Lost update
![Lost update](https://i2.paste.pics/e398cc2c882caa626962add05654a57a.png "Lost update")
### Non-repeatable read with READ COMMITED
![Non-repeatable read with read commited](https://i2.paste.pics/a5a0709b99ddbccf18020282e8716a5f.png "Non-repeatable read with READ COMMITED")
### Non-repeatable with REPEATABLE READ
![Non-repeatable with REPEATABLE READ](https://i2.paste.pics/44c62aff221e87e32f822c972016a323.png "Non-repeatable with REPEATABLE READ")
### Phantom reads with READ COMMITED
![Phantom reads with READ COMMITED](https://i2.paste.pics/18cf6a52bb774b24b9aea3731a172822.png "Phantom reads with READ COMMITED")
### Phantom reads with SERIALIZABLE
![Phantom reads with SERIALIZABLE](https://i2.paste.pics/aeb84536a084ed8066f36e5e64a1f948.png "Phantom reads with SERIALIZABLE")

