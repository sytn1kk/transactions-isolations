# Transaction 1

START TRANSACTION;
UPDATE app.BankDetailTbl
    SET Balance = Balance + 12
WHERE id = 1;
DO SLEEP(20);
ROLLBACK;

SELECT * FROM app.BankDetailTbl WHERE id = 1;


# Transaction 2

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SELECT * FROM app.BankDetailTbl WHERE id = 1;
COMMIT;